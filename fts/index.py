import datetime as dt

import numpy as np
import pandas as pd
from sklearn import neural_network as nn, svm

from pandas_datareader.data import get_data_yahoo as _fetch

from fts.mpdist import MP


class Fetch:
    """
    >>> f = Fetch("https://raw.githubusercontent.com/datasets/s-and-p-500-companies/master/data/constituents.csv", column='Symbol')
    >>> f.fetch()
    """
    def __init__(self, symbols, start=None, end=None, column=None, sep=","):
        self._raw_df = pd.DataFrame()

        if column:
            self._symbols = pd.read_csv(symbols, sep=sep)[column]
        else:
            self._symbols = pd.read_csv(symbols, sep=sep)

        self._start = start or dt.date.today()-dt.timedelta(365)
        self._end = end or dt.date.today()


    def __call__(self):
        if self._raw_df.empty is True:
            self.fetch()
        return self._raw_df


    def fetch(self):
        self._raw_df = \
            pd.concat([_fetch(sym, start=self._start, end=self._end)['Adj Close'].to_frame(sym) for sym in self._symbols], axis=1)


    def save(self, fname):
        self._raw_df.to_csv("{}_{}_{}.csv".format(fname, self._start, self._end))


class NegativeLagException(Exception):
    pass

class Index:
    """

    """
    def __init__(self, data=None, symbols=None, *args, **kwargs):
        csv = kwargs.get('csv')

        if data is None:
            self._raw_data = pd.read_csv(csv, index_col='Date')
        else:
            self._raw_data = data

        self._raw_data = self._raw_data.dropna('columns')

        self._returns = pd.DataFrame()
        self._corr = dict()
        self._eigvect = dict()
        self._eigvals = dict()
        self._clean_corr = dict()


    def _compute_log_returns(self):
        log = self._raw_data.apply(np.log)
        self._returns = pd.DataFrame(log[1:].values-log[:-1].values,
                                     index=log[1:].index,
                                     columns=log[1:].columns)


    def _compute_corr(self, lag):
        if lag > 0:
            ret = self.returns()
            corr = ret[lag:].apply(lambda s: ret[:-lag].corrwith(s))
        elif lag == 0:
            corr = self.returns().corr()
        else:
            raise NegativeLagException("Negative lag is anticipative")

        self._corr.update({lag: corr})


    def _compute_eigvals(self, lag):
        self._eigvals[lag] = np.linalg.eigvals(self.corr(lag))


    def _compute_eigvect(self, lag):
        self._eigvals[lag], self._eigvect[lag] = np.linalg.eig(self.corr(lag))

    def _compute_clean_corr(self, lag):
        D = np.diag(np.concatenate(self.mp_ev_decomp(lag)))
        P = np.matrix(self.eigvect(lag))

        if lag == 0:
            cc = P*D*P.transpose()
        else:
            cc = P*D*np.linalg.inv(P)
            
        self._clean_corr.update({lag: cc})

    def returns(self):
        if self._returns.empty is True:
            self._compute_log_returns()
        return self._returns


    def corr(self, lag=0):
        if lag not in self._corr:
            self._compute_corr(lag)
        return self._corr[lag]


    def eigvect(self, lag=0):
        if lag not in self._eigvect:
            self._compute_eigvect(lag)
        return self._eigvect[lag]


    def eigvals(self, lag=0):
        if lag not in self._eigvals:
            self._compute_eigvals(lag)
        return self._eigvals[lag]


    def mp_ev_decomp(self, lag=0):
        rows, cols = self.data().shape

        d,(lm,lp) = MP(1, rows, cols)

        ev = self.eigvals(lag)

        sev = ev[ev > lp]
        nev = ev[ev <= lp]
        mev = np.mean(nev)*np.ones(len(nev))

        return (sev, mev)


    def clean_corr(self, lag=0):
        if lag not in self._clean_corr:
            self._compute_clean_corr(lag)
        return self._clean_corr[lag]


    def data(self):
        return self._raw_data


class NNDataIndex(Index):

    def __init__(self, data):
        super().__init__(data)
        self._data = dict()
        self._target = dict()
        self._clf = dict()
        self._clf2 = dict()

    def learning_data(self, stock_sym, threshold=0):
        """
        Example
        """

        if threshold not in self._data:
            ret = self.returns()
            ret[ret <= threshold] = -1
            ret[ret  > threshold] = 1
        
            self._data[threshold] = ret
        
        return (self._data[threshold][:-1],
                self._data[threshold][stock_sym][1:])


    def _nn_classify2(self, lag, data, targets):
        #for d,t in zip(data.values(), targets.values()):
        self._clf2[lag] = dict()
        for (k,d) in data.items():
            t = targets[k]
            self._clf2[lag][k] = svm.SVC(gamma=0.001, C=100.)
            self._clf2[lag][k].fit(d[:-lag], t[:-lag])

    def _nn_classify(self, lag, data, targets):
        #for d,t in zip(data.values(), targets.values()):
        self._clf[lag] = dict()
        for (k,d) in data.items():
            t = targets[k]
            self._clf[lag][k] = nn.MLPClassifier(max_iter=1000, verbose=True, hidden_layer_sizes=(60,5))
            self._clf[lag][k].fit(d[:-lag], t[:-lag])


    def nn_classifier2(self, lag, stocks=[], threshold=0):
        _stocks = stocks or self.data().keys()

        if lag not in self._clf2:
            self._clf2[lag] = dict()
            data, targets = dict(), dict()
            for i in _stocks:
                data[i], targets[i] = self.learning_data(i, threshold)
            self._nn_classify2(lag, data, targets)

    def nn_classifier(self, lag, stocks=[], threshold=0):
        _stocks = stocks or self.data().keys()

        if lag not in self._clf:
            data, targets = dict(), dict()
            for i in _stocks:
                data[i], targets[i] = self.learning_data(i, threshold)
            self._clf[lag] = dict()
            self._nn_classify(lag, data, targets)

        return self._clf[lag]

    def results(self, lag, threshold=0):
        ds = map(self.learning_data, self._clf[lag].keys())
        return {s: np.mean(v.predict(d[-lag:]) == t[-lag:]) 
                for (s,v),(d,t) in zip(self._clf[lag].items(), ds)}
