import math


def MP(s,m,n):
    l = m/n
    sq = math.pow(s,2)
    l_sr = math.sqrt(l)

    lp, lm = sq*math.pow(1+l_sr,2), sq*math.pow(1-l_sr, 2)

    def d(x):
        if x >= lm and x <= lp:
            return 1/(math.pi*sq*2)*math.sqrt((lp-x)*(x-lm))/(l*x)
        else:
            return 0
    
    if l >= 0 and l <= 1:
        return (d, (lm,lp))
    elif l > 1:
        a = 1-1/l
        return (lambda x: a+d(x), (lm,lp))

