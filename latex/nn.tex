\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{dsfont}

\usepackage{svg}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{tikz}

\author{MB}
\begin{document}

\section{Apprentissage statistique et modèle linéaire général}

Nous utiliserons un réseau de neurone à 5 couches et 40 neurones par couches pour tenter de prévoir les montées et déscentes du CAC40 et du S\&P500, en utilisant la fonction d'activation ReLU ($max(0,x)$).

Ce réseau de neurones correspond à un modèle linéaire général Tobit.

On construit les \textit{features} en considérant les log-rendements du S\&P500 entre 2013 et aujourd'hui puis en changeant les valeurs en $-1$ (resp. $1$) selon que le log-rendement est négatif (resp. positif).

On effectue l'apprentissage statistique supervisé en considérant les couples de log-rendements des dates $t_{i-1}$ et $t_i$.

Pour ce faire, l'échantillon d'apprentissage est découpé en deux parties, une servant à l'apprentissage et l'autre à la validation, en faisant très attention à garder le caractère progressif du processus des prix et log-rendements.

La procédure consiste donc à faire :

\begin{enumerate}
\item apprendre sur un échantillon privé de la journée $t_{i}$,
\item prévoir la journée suivante $t_i$,
\item isoler les actifs avec un bon score (supérieur à un seuil $s$),
\item réapprendre la journée manquante $t_i$,
\item puis prévoir la journée suivante $t_{i+1}$ pour les actifs isolés
\item étudier le processus d'autocorrélation pour déduire les tendances des autres actifs
\end{enumerate} 

\begin{table}[h]
\centering

\begin{tabular}{|c|c|}
\hline
Seuil & Score \\
\hline
0.5 & $54\%$ \\
0.6 & $58\%$ \\
\hline
\end{tabular}
\caption{Rolling journalier S\&P500}
\label{tab:cac}

\end{table}

Pour les scores proches de $50\%$ autant décider en lançant une pièce équilibrée et éviter tout travail inutile.
Pouvait-on prévoir des résultats aussi faibles ? La réponse est a posteriori oui car nous manquons de données pour que le Machine Learning prenne tout son sens.
De plus, combien même si nous disposons de grandes données, faut-il qu'elles soient suffisamment discriminantes pour espérer déceler des motifs ou des tendances.

\newpage

\section{Exploitation des résultats}

Si l'on arrive à déterminer les tendances d'une classe particulière d'actifs d'une journée à une autre, on peut alors s'en servir comme proxies pour dégager les tendances des actifs restants en étudiant la corrélation ou la corrélation retardée.

Par ailleurs, nul besoin de calculs fastidieux pour entrevoir que les courbes de P\&L vont fortement se dégrader si nous considérons les frais de transaction et les frais induits par notre \textit{Market Impact} quant aux différentes stratégies liquidatives et de rebalancement du portefeuille, si nous adoptions une stratégie naïve d'achat et vente.

Mais alors que faire ?

\paragraph{Amélioration du Ratio Sharpe}
Dans le cadre Markowitz de l'optimisation de portefeuille d'actifs, nous pouvons prétendre à améliorer son rendement au delà du Ratio Sharpe du marché.

En effet, nous pouvons injecter l'information des tendances ainsi obtenues conjuguées à la variance des actifs pour rééquilibrer le portefeuille.

\paragraph{Amélioration de la tracking error} Dans le Hedging d'options dynamique, on peut utiliser l'information obtenue pour rebalancer le portefeuille autofinancé de manière anticipative, toujours en conjugaison avec la variance des actifs.

Nous pourrions ainsi éliminer les pertes liées au retard dû aux pas de discrétisation.


Quel que soit le cas de figure, l'estimation de la matrice de variance est primordiale.

\section{Estimation de la matrice d'autocorrélation,\\ Marchenko-Pastur}

L'estimateur naturel utilisé est celui de la variance empirique :
\begin{equation}
\hat{\Omega}_N := X^TX
\end{equation}

Si $X$ est la matrice $T \times N$ de nos données.

Mais la dimension de nos données rend notre estimateur bruité.

Le nombre d'actifs $N$ étant de l'ordre de grandeur des journées observées, l'estimateur empirique de la matrice d'(auto)-corrélation est extrêmement bruitée.

Examinons en détail la matrice de corrélation de $1000$ trajectoires de $10000$ points, gaussiennes centrées et de variance $5$.

\begin{figure}[h]
\subfloat[Distribution des valeurs propres de la matrice de corrélation de 1000 trajectoires de 10000 points tirés suivant une $\mathcal{N}(0,5)$]
{
    \includegraphics[scale=0.4]{../mp_n_5_1000p_10000v}
    \label{fig:mpn}
}
\quad
\subfloat[Distribution des valeurs propres de la matrice de corrélation du S\&P500 entre 2013 et 2017]
{
    \includegraphics[scale=0.4]{../snp500_eigval_dist}
    \label{fig:snp}
}

\caption{Distributions de valeurs propres}
\end{figure}

\ref{fig:mpn} nous montre que dans le cadre \textit{iid} en l'absence donc d'information, les valeurs propres de la matrice de corrélation sont distribuées suivant la loi de Marchenko-Pastur :

\begin{equation}
\frac{1}{2 \pi} \frac{ \sqrt{(\lambda_+ - x)(x - \lambda_-)} }{\lambda x} \mathds{1}_{[\lambda_-, \lambda_+]} 
\end{equation}

où $\lambda_\pm = (1 \pm \sqrt{\lambda})^2$ et $\lambda \approx \frac{N}{T}$

La figure \ref{fig:snp} quant à elle nous montre que les petites valeurs propres de l'estimateur de la corrélation du S\&P500 sont importantes en nombre, et vont donc jouer un rôle extrêmement important si on inverse la matrice $\hat{\Omega}_N$.

La procédure de nettoyage consiste donc à éliminer les valeurs propres inférieures à $\lambda_+$ puisqu'elles sont de l'ordre du bruit et les remplacer par exemple par leur moyenne afin de conserver le rang de la matrice.

\begin{figure}[h]
\subfloat[Vue de la matrice de corrélation du S\&P500 de 2013 à 2017 avant nettoyage] { \includegraphics[scale=0.5]{../snp500_corr} }
\qquad
\subfloat[Vue de la matrice de corrélation du S\&P500 de 2013 à 2017 après nettoyage] { \includegraphics[scale=0.5]{../snp500_corr_mp} }
\end{figure}

Il faut garder à l'esprit que la nouvelle matrice ainsi obtenue n'est pas forcément une matrice de corrélation à proprement parler, mais l'information apportée est plus pertinente.

\newpage

\section{Pour aller au delà du cours}

\begin{figure}[h]
\centering
\begin{tikzpicture}
\pgfmathsetmacro\scale{1.15}

\foreach \y in {1,2,3}  
  \node[draw,circle] (I_\y) at (0,\scale*\y + \scale*0.5) {$O_\y$};

\foreach \y in {1,2,3,4}  
  \node[draw,circle] (H1_\y) at (2,\scale*\y) {$H^1_\y$};

\foreach \y in {1,2,3}  
  \node[draw,circle] (HA_\y) at (4,\scale*\y + \scale*0.5) {$f_\y$};

\node[draw,circle] (AC) at (4,\scale*-0.25) {AC};

\foreach \y in {1,2,3,4}  
  \node[draw,circle] (H2_\y) at (6,\scale*\y) {$H^2_\y$};

\foreach \y in {2,3}
  \node[draw,circle] (O_\y) at (8,\scale*\y) {$O_\y$};
  
\foreach \o in {1,2,3}
   \foreach \h in {1,2,3,4}
      \draw[->,>=latex] (I_\o) -- (H1_\h);

\foreach \h in {1,2,3,4}
   \foreach \ha in {1,2,3}
       \draw[->,>=latex] (H1_\h) -- (HA_\ha);

\foreach \f in {1,2,3}
   \foreach \h in {1,2,3,4}
      \draw[->,>=latex] (HA_\f) -- (H2_\h);

\foreach \h in {1,2,3,4}
   \foreach \o in {2,3}
      \draw[->,>=latex] (H2_\h) -- (O_\o);
  
\foreach \o in {2,3}
    \draw[->,>=latex] (O_\o) to[bend left=\o*20] (AC);
    
\draw[->,>=latex] (AC) to[bend left=20] (I_1);
\end{tikzpicture}
\end{figure}

Pour aller au delà du cours : on utilise un réseau de neurone adaptatif récurrent, voire du Deep Reinfocement Learning et on le nourrit des sorties du réseau ajustées par l'autocorrélation.

On peut également ajouter un decay pour mettre moins d'importance aux anciennes valeurs de sorte à capturer d'un côté les propriétés stationnaires du marché et de l'autre son caractère volatile à court terme en mettant l'accent sur les valeurs fraiches.

\end{document}
