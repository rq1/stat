import os

FOLDERS = {
    'project_root': os.path.realpath('..'),
    'tickers_folder': 'tickers',
    'raw_data': 'rawd',
    'data': 'data'
}

def ppath(k):
    return os.path.join(FOLDERS['project_root'], FOLDERS[k])
