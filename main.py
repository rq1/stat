import numpy as np
import matplotlib.pyplot as plt
import datetime as dt

from fts.index import Fetch, Index
import fts.mpdist as mp


start = dt.datetime(2013, 1, 1)
end = dt.datetime(2017, 1, 1)

cacsym = Fetch("./tickers/cac40.csv", column="yf_ticker", start=start, end=end)
snpsym = Fetch("https://raw.githubusercontent.com/datasets/s-and-p-500-companies/master/data/constituents.csv", column='Symbol', start=start, end=end)
nsdsym = Fetch("http://www.nasdaq.com/quotes/nasdaq-100-stocks.aspx?render=download", column='Symbol', start=start, end=end)


cac = Index(data=cacsym())
snp = Index(data=snpsym())
nsd = Index(data=nsdsym())

#cac_ev_hist = plt.hist(cac.eigvals())
#snp_ev_hist = plt.hist(snp.eigvals())
#
#plt.show()
