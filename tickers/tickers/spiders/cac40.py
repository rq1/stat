# -*- coding: utf-8 -*-
import scrapy
import re


class Cac40Spider(scrapy.Spider):
    name = "cac40"
    allowed_domains = ["euronext.com"]
    start_urls = ['https://www.euronext.com/fr/products/indices/FR0003500008-XPAR/market-information']
    YPLACE = {'PAR': 'PA',
              'BRU': 'BR',
              'AMS': 'AS'}

    def parse(self, response):
        for href in response.css('#nyx_ic_block td a::attr(href)').extract():
            place = re.findall(r'X([A-Z]{3})', href)[0]
            yield scrapy.Request(response.urljoin(href),
                                 callback=self.parse_ticker,
                                 meta={'place': place})

    def parse_ticker(self, response):
        def extract_with_css(query):
            print("CACA:",response.url)
            return response.css(query).extract_first().strip()

        symbol = extract_with_css('#sidebar-second .symbol span::text')
        raw_place = response.meta['place']
        gf_ticker = "E{}:{}".format(raw_place[:2], symbol)
        yf_ticker = "{}.{}".format(symbol, self.YPLACE[raw_place])

        yield {
            'symbol': symbol,
            'place': raw_place,
            'gf_ticker': gf_ticker,
            'yf_ticker': yf_ticker
        } 
